public type LearningObjectMedia record {
    LearningObjectMediaArray audio?;
    LearningObjectMediaArray text?;
    LearningObjectMediaArray video?;
};

#public type LearningObjectMedia record {
#    string name?;
#    string description?;
#    string difficulty?;
#};

public type LearningObjectMediaArray record {string name?; string description?; string difficulty?;}[];

public type LearningMaterial record {
    string course?;
    record  { LearningObjectMedia required; LearningObjectMedia suggested;}  learning_objects?;
};

public type Learner record {
    string username;
    string firstname?;
    string lastname?;
    string[] preferred_formats?;
    record  { string course; string score;} [] past_subjects;
};
