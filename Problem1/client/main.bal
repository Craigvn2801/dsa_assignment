import ballerina/io;

public function main() returns error? {
    Client vleClient = check new Client();

    string testUsername = "JBrandt";
    string testNewLastName = "Brandt";
    Learner newTestUser = {
        firstname: "Nate",
        lastname: "Ballard",
        username: "ballardnate",
        past_subjects: [],
        preferred_formats: []
    };


    Learner[] learners = check vleClient->list();
    io:println("List all learners:\n " + learners.map((s) => s["firstname"]).toString() + "\n");


    Learner learner = check vleClient->getUserByusername(testUsername);
    io:println("Full details of user " + testUsername + ":\n ", learner, "\n");

    Learner updatedUser = learner;

    updatedUser["lastname"] = testNewLastName;
    io:println("Changing user " + testUsername + "'s last name to '" + testNewLastName + "' ...");
    check vleClient->update(testUsername, updatedUser);

    learner = check vleClient->getUserByusername(testUsername);
    io:println("New last name: " + learner["lastname"].toString() + "\n");


    io:println("Creating learner profile: " + newTestUser["firstname"].toString() + " " + newTestUser["lastname"].toString() + " (" + newTestUser["username"] + ") ...");
    check vleClient->create(newTestUser);

    learner = check vleClient->getUserByusername(newTestUser.username);
    io:println("Found created user: '" + newTestUser["username"] + "'\n");


    var learningMaterial = check vleClient->getLearningMaterial(testUsername, "DSA621S");
    io:println(learningMaterial);
}
