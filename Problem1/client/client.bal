import  ballerina/http;
import  ballerina/url;
import  ballerina/lang.'string;

public type LearnerArr Learner[];

# API for Assignment Probelem 1
#
# + clientEp - Connector http endpoint
public client class Client {
    http:Client clientEp;
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "http://localhost:9090/v1") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
    }
    #
    # + return - learner created successfully
    remote isolated function  create(Learner payload) returns error? {
        string  path = string `/learners/create`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        _ = check self.clientEp-> post(path, request, targetType=http:Response);
    }
    #
    # + return - learner profile successfully
    remote isolated function  update(string username, Learner payload) returns error? {
        string  path = string `/learners/update`;
        map<anydata> queryParam = {"username": username};
        path = path + check getPathForQueryParam(queryParam);
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        _ = check self.clientEp-> post(path, request, targetType=http:Response);
    }
    #
    # + return - list of learners successfully returned
    remote isolated function  list() returns LearnerArr|error {
        string  path = string `/learners/list`;
        LearnerArr response = check self.clientEp-> get(path, targetType = LearnerArr);
        return response;
    }
    #
    # + return - learner profile successfully updated
    remote isolated function  getLearningMaterial(string username, string courseId) returns LearningMaterial?|error {
        string  path = string `/learners/getLearningMaterial`;
        map<anydata> queryParam = {"username": username, "courseId": courseId};
        path = path + check getPathForQueryParam(queryParam);
        http:Request request = new;
        //TODO: Update the request as needed;
        LearningMaterial? response = check self.clientEp-> post(path, request, targetType = LearningMaterial);
        return response;
    }
    #
    # + return - learners returned successfully
    remote isolated function  getUserByusername(string username) returns Learner|error {
        string  path = string `/learners/getUser/${username}`;
        Learner response = check self.clientEp-> get(path, targetType = Learner);
        return response;
    }
}

# Generate query path with query parameter.
#
# + queryParam - Query parameter map
# + return - Returns generated Path or error at failure of client initialization
isolated function  getPathForQueryParam(map<anydata>   queryParam)  returns  string|error {
    string[] param = [];
    param[param.length()] = "?";
    foreach  var [key, value] in  queryParam.entries() {
        if  value  is  () {
            _ = queryParam.remove(key);
        } else {
            if  string:startsWith( key, "'") {
                param[param.length()] = string:substring(key, 1, key.length());
            } else {
                param[param.length()] = key;
            }
            param[param.length()] = "=";
            if  value  is  string {
                string updateV =  check url:encode(value, "UTF-8");
                param[param.length()] = updateV;
            } else {
                param[param.length()] = value.toString();
            }
            param[param.length()] = "&";
        }
    }
    _ = param.remove(param.length()-1);
    if  param.length() ==  1 {
        _ = param.remove(0);
    }
    string restOfPath = string:'join("", ...param);
    return restOfPath;
}
