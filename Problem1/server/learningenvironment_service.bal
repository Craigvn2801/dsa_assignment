import  ballerina/http;
// import ballerina/io;
#import ballerina/log;

map<int> scoreInt = {
    "A+": 100,
    "A": 95,
    "A−": 90,
    "B+": 87,
    "B": 83,
    "B−": 80,
    "C+": 77,
    "C": 75,
    "C−": 70,
    "D+": 67,
    "D": 65,
    "D−": 60,
    "F": 55
};

Learner[] learners = [
    {
        username: "KCvanNiekerk",
        lastname: "Van Niekerk",
        firstname: "Keith",
        preferred_formats: ["audio", "video", "text"],
        past_subjects: [
            {
                course: "Dark Arts",
                score: "C+"
            },
            {  
            course: "Web Application Development",
            score: "B-"
            }
        ]
    },

{
        username: "JBrandt",
        lastname: "Brandt",
        firstname: "Justin",
        preferred_formats: ["audio", "video", "text"],
        past_subjects: [
            {
                course: "Programming 2",
                score: "C+"
            },
            {  
            course: "Web Application Development",
            score: "B-"
            }
        ]
    },

    {
        username: "HPotter",
        lastname: "Potter",
        firstname: "Harry",
        preferred_formats: ["audio", "video", "text"],
        past_subjects: [
            {
            course: "Charms",
            score: "C-"
            },
            {
            course: "Potions",
            score: "D-"
            }
        ]
    },

    {
        username: "hGranger",
        lastname: "Granger",
        firstname: "Hermione",
        preferred_formats: ["audio", "video", "text"],
        past_subjects: [
            {
                course: "Potions",
                score: "D+"
            },
            {
                course: "History of Magic",
                score: "A+"
            }
        ]
    },
    
    {
        username: "lLovegood",
        lastname: "Lovegoog",
        firstname: "Luna",
        preferred_formats: ["audio", "video", "text"],
        past_subjects: [
            {
            course: "Transfiguration",
            score: "D-"
            },
            {
            course: "Astronomy",
            score: "F"
            }
        ]
    },
    
    {
        username: "cMcLaggen",
        lastname: "McLaggen",
        firstname: "Cormac",
        preferred_formats: ["audio", "video", "text"],
        past_subjects: [
            {
            course: "Defense against the dark arts",
            score: "B+"
            },
            {
            course: "Astronomy",
            score: "A+"
            }
        ]
    }
];

map<Course> courses = {
    "DSA621S": {
        id: "DSA621S",
        name: "Distributed Systems Applications",
        learning_objects: [
        {
            name: "Topic 1",
            description: "",
            difficulty: "Easy",
            mediaType: MEDIA_AUDIO,
            requirements: [
            {
                subject: "Programming 2",
                max_score: "B",
                min_score: "F"
            }
            ]
        }, 
        {
            name: "Topic 2",
            description: "",
            difficulty: "Medium",
            mediaType: MEDIA_AUDIO,
            requirements: [
            {
                subject: "Programming 2",
                max_score: "A+",
                min_score: "C"
            }
            ]
        }
        ]
    }
};

listener  http:Listener  ep0  = new (9090, config  = {host: "localhost"});

function findLearner(string username) returns Learner? {
    Learner? foundLearner = ();
    foreach var learner in learners {
        if learner.username == username {
            foundLearner = learner;
            break;
        }
    }
    return foundLearner;
}

function findCourse(string courseId) returns Course? {
    foreach var course in courses {
        if course.id == courseId {
            return course;
        }
    }
    return ();
}

function findPastSubject(Learner learner, string course) returns PastSubject? {
    foreach PastSubject pastSubject in learner.past_subjects {
        if pastSubject?.course == course {
            return pastSubject;
        }
    }
    return ();
}

function learnerMeetsRequirements(Learner learner, LearningObjectRequirement requirement) returns boolean {
    PastSubject? pastSubject = findPastSubject(learner, requirement.subject);
    if !(pastSubject is ()) {
        int? score = scoreInt[pastSubject.score];
        int? minScore = scoreInt[requirement.min_score];
        int? maxScore = scoreInt[requirement.max_score];
        if (!(score is ()) && !(minScore is ()) && !(maxScore is ())) {
            if (score >= minScore && score <= maxScore) {
                return true;
            }
        }
    }
    return false;
}

function mediaTypePreferred(Learner learner, MediaType mediaType) returns boolean {
    var preferred_formats = learner?.preferred_formats;
    if preferred_formats != () {
        int? idx = preferred_formats.indexOf(mediaType);
        return idx is int;
    }
    return false;
}

service  /v1  on  ep0  {
        resource  function  post  learners/create(@http:Payload  {} Learner  payload)  returns  http:Ok {
        learners.push(payload);
        return <http:Ok>{};
    }
        resource  function  post  learners/update(string  username, @http:Payload  {} Learner  payload)  returns  http:Ok {
        if !(payload?.firstname is ()) 
        && !(payload?.lastname is ()) 
        && !(payload?.preferred_formats is ()) {
            Learner? foundLearner = findLearner(username);
            if !(foundLearner is ()) {
                foundLearner.firstname = payload?.firstname ?: "";
                foundLearner.lastname = payload?.lastname ?: "";
                foundLearner.past_subjects = payload.past_subjects;
                foundLearner.preferred_formats = payload?.preferred_formats ?: [];
            } else {
            }
        }
        return <http:Ok>{};
    }

        resource  function  get  learners/list()  returns  Learner[] {
        return learners;
    }

        resource  function  post  learners/getLearningMaterial(string  username, string  courseId)  returns  LearningMaterial? {
        Learner? learner = findLearner(username);
        Course? course = findCourse(courseId);
        if learner is () || course is () {
            if learner is () {
                return ();
            }
            if course is () {
            }
        } else {
            LearningObject learningObjectResult = {
                audio: [],
                text: [],
                video: []
            };
            foreach var learningObject in course.learning_objects {
                boolean isPreferredFormat = mediaTypePreferred(learner, learningObject.mediaType);
                if !isPreferredFormat {
                    continue;
                }
                boolean requirementsMet = true;
                foreach var requirement in learningObject.requirements {
                    boolean qualifiesForCourse = learnerMeetsRequirements(learner, requirement);
                    if (!qualifiesForCourse) {
                        requirementsMet = false;
                        break;
                    }
                }
                if (requirementsMet) {
                    var mediaArr = learningObjectResult[learningObject.mediaType];
                    if (!(mediaArr is ())) {
                        mediaArr.push(learningObject);
                    }
                }
            }
            LearningMaterial mat = {
                course: course.name,
                learning_objects: {
                    required: learningObjectResult,
                    suggested: {}
                }
            };
            return mat;
        }
    }

        resource  function  get  learners/getUser/[string  username]()  returns  Learner|error {
        Learner? learner = findLearner(username);
        if learner is () {
            return error("Learner not found");
        } else {
            return learner;
        }
    }
}
