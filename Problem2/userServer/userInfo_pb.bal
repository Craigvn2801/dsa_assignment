import ballerina/grpc;

public isolated client class FunctionManagerClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(add_new_func|ContextAdd_new_func req) returns (g_Response|grpc:Error) {
        map<string|string[]> headers = {};
        add_new_func message;
        if (req is ContextAdd_new_func) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <g_Response>result;
    }

    isolated remote function add_new_fnContext(add_new_func|ContextAdd_new_func req) returns (ContextG_Response|grpc:Error) {
        map<string|string[]> headers = {};
        add_new_func message;
        if (req is ContextAdd_new_func) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <g_Response>result, headers: respHeaders};
    }

    isolated remote function delete_fn(delete_func|ContextDelete_func req) returns (g_Response|grpc:Error) {
        map<string|string[]> headers = {};
        delete_func message;
        if (req is ContextDelete_func) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <g_Response>result;
    }

    isolated remote function delete_fnContext(delete_func|ContextDelete_func req) returns (ContextG_Response|grpc:Error) {
        map<string|string[]> headers = {};
        delete_func message;
        if (req is ContextDelete_func) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <g_Response>result, headers: respHeaders};
    }

    isolated remote function show_fn(show_func|ContextShow_func req) returns (func_version|grpc:Error) {
        map<string|string[]> headers = {};
        show_func message;
        if (req is ContextShow_func) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <func_version>result;
    }

    isolated remote function show_fnContext(show_func|ContextShow_func req) returns (ContextFunc_version|grpc:Error) {
        map<string|string[]> headers = {};
        show_func message;
        if (req is ContextShow_func) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <func_version>result, headers: respHeaders};
    }

    isolated remote function add_fns() returns (Add_fnsStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("FunctionManager/add_fns");
        return new Add_fnsStreamingClient(sClient);
    }

    isolated remote function show_all_fns(show_all_func|ContextShow_all_func req) returns stream<func_version, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        show_all_func message;
        if (req is ContextShow_all_func) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("FunctionManager/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        Func_versionStream outputStream = new Func_versionStream(result);
        return new stream<func_version, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext(show_all_func|ContextShow_all_func req) returns ContextFunc_versionStream|grpc:Error {
        map<string|string[]> headers = {};
        show_all_func message;
        if (req is ContextShow_all_func) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("FunctionManager/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        Func_versionStream outputStream = new Func_versionStream(result);
        return {content: new stream<func_version, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria() returns (Show_all_with_criteriaStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("FunctionManager/show_all_with_criteria");
        return new Show_all_with_criteriaStreamingClient(sClient);
    }
}

public client class Add_fnsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendFunc(func message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextFunc(ContextFunc message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveG_Response() returns g_Response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <g_Response>payload;
        }
    }

    isolated remote function receiveContextG_Response() returns ContextG_Response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <g_Response>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class Func_versionStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|func_version value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|func_version value;|} nextRecord = {value: <func_version>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Show_all_with_criteriaStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendShow_all_with_criteria_a(show_all_with_criteria_a message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextShow_all_with_criteria_a(ContextShow_all_with_criteria_a message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveShow_all_with_criteria_response() returns show_all_with_criteria_response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <show_all_with_criteria_response>payload;
        }
    }

    isolated remote function receiveContextShow_all_with_criteria_response() returns ContextShow_all_with_criteria_response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <show_all_with_criteria_response>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class FunctionManagerGResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendG_Response(g_Response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextG_Response(ContextG_Response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionManagerShowallwithcriteriaresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendShow_all_with_criteria_response(show_all_with_criteria_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextShow_all_with_criteria_response(ContextShow_all_with_criteria_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionManagerFuncversionCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFunc_version(func_version response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFunc_version(ContextFunc_version response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextShow_all_with_criteria_responseStream record {|
    stream<show_all_with_criteria_response, error?> content;
    map<string|string[]> headers;
|};

public type ContextFuncStream record {|
    stream<func, error?> content;
    map<string|string[]> headers;
|};

public type ContextFunc_versionStream record {|
    stream<func_version, error?> content;
    map<string|string[]> headers;
|};

public type ContextShow_all_with_criteria_aStream record {|
    stream<show_all_with_criteria_a, error?> content;
    map<string|string[]> headers;
|};

public type ContextShow_all_with_criteria_response record {|
    show_all_with_criteria_response content;
    map<string|string[]> headers;
|};

public type ContextShow_func record {|
    show_func content;
    map<string|string[]> headers;
|};

public type ContextFunc record {|
    func content;
    map<string|string[]> headers;
|};

public type ContextDelete_func record {|
    delete_func content;
    map<string|string[]> headers;
|};

public type ContextShow_all_func record {|
    show_all_func content;
    map<string|string[]> headers;
|};

public type ContextG_Response record {|
    g_Response content;
    map<string|string[]> headers;
|};

public type ContextFunc_version record {|
    func_version content;
    map<string|string[]> headers;
|};

public type ContextShow_all_with_criteria_a record {|
    show_all_with_criteria_a content;
    map<string|string[]> headers;
|};

public type ContextAdd_new_func record {|
    add_new_func content;
    map<string|string[]> headers;
|};

public type show_all_with_criteria_response record {|
    func 'function = {};
    string message = "";
|};

public type show_func record {|
    string funcId = "";
    string funcVersionId = "";
|};

public type func record {|
    string Id = "";
    string Name = "";
    func_version[] Versions = [];
|};

public type delete_func record {|
    string functionId = "";
|};

public type show_all_func record {|
    string funcId = "";
|};

public type func_developer record {|
    string Name = "";
    string Email = "";
|};

public type func_version record {|
    string Id = "";
    func_developer Developer = {};
    string lang = "";
    string[] Keywords = [];
    string Contents = "";
|};

public type g_Response record {|
    string Message = "";
|};

public type show_all_with_criteria_a record {|
    string lang = "";
    string[] keywords = [];
|};

public type add_new_func record {|
    string funcId = "";
    func_version functionVersion = {};
    string funcName = "";
|};

const string ROOT_DESCRIPTOR = "0A0E75736572496E666F2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F223A0A0E66756E635F646576656C6F70657212120A044E616D6518012001280952044E616D6512140A05456D61696C1802200128095205456D61696C2299010A0C66756E635F76657273696F6E120E0A02496418012001280952024964122D0A09446576656C6F70657218022001280B320F2E66756E635F646576656C6F7065725209446576656C6F70657212120A046C616E6718032001280952046C616E67121A0A084B6579776F72647318042003280952084B6579776F726473121A0A08436F6E74656E74731805200128095208436F6E74656E747322550A0466756E63120E0A0249641801200128095202496412120A044E616D6518022001280952044E616D6512290A0856657273696F6E7318032003280B320D2E66756E635F76657273696F6E520856657273696F6E7322260A0A675F526573706F6E736512180A074D65737361676518012001280952074D657373616765222D0A0B64656C6574655F66756E63121E0A0A66756E6374696F6E4964180120012809520A66756E6374696F6E4964227B0A0C6164645F6E65775F66756E6312160A0666756E634964180120012809520666756E63496412370A0F66756E6374696F6E56657273696F6E18022001280B320D2E66756E635F76657273696F6E520F66756E6374696F6E56657273696F6E121A0A0866756E634E616D65180320012809520866756E634E616D6522490A0973686F775F66756E6312160A0666756E634964180120012809520666756E63496412240A0D66756E6356657273696F6E4964180220012809520D66756E6356657273696F6E496422270A0D73686F775F616C6C5F66756E6312160A0666756E634964180120012809520666756E634964224A0A1873686F775F616C6C5F776974685F63726974657269615F6112120A046C616E6718012001280952046C616E67121A0A086B6579776F72647318022003280952086B6579776F726473225E0A1F73686F775F616C6C5F776974685F63726974657269615F726573706F6E736512210A0866756E6374696F6E18012001280B32052E66756E63520866756E6374696F6E12180A076D65737361676518022001280952076D65737361676532B6020A0F46756E6374696F6E4D616E6167657212280A0A6164645F6E65775F666E120D2E6164645F6E65775F66756E631A0B2E675F526573706F6E7365121F0A076164645F666E7312052E66756E631A0B2E675F526573706F6E7365280112260A0964656C6574655F666E120C2E64656C6574655F66756E631A0B2E675F526573706F6E736512240A0773686F775F666E120A2E73686F775F66756E631A0D2E66756E635F76657273696F6E122F0A0C73686F775F616C6C5F666E73120E2E73686F775F616C6C5F66756E631A0D2E66756E635F76657273696F6E300112590A1673686F775F616C6C5F776974685F637269746572696112192E73686F775F616C6C5F776974685F63726974657269615F611A202E73686F775F616C6C5F776974685F63726974657269615F726573706F6E736528013001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "userInfo.proto": "0A0E75736572496E666F2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F223A0A0E66756E635F646576656C6F70657212120A044E616D6518012001280952044E616D6512140A05456D61696C1802200128095205456D61696C2299010A0C66756E635F76657273696F6E120E0A02496418012001280952024964122D0A09446576656C6F70657218022001280B320F2E66756E635F646576656C6F7065725209446576656C6F70657212120A046C616E6718032001280952046C616E67121A0A084B6579776F72647318042003280952084B6579776F726473121A0A08436F6E74656E74731805200128095208436F6E74656E747322550A0466756E63120E0A0249641801200128095202496412120A044E616D6518022001280952044E616D6512290A0856657273696F6E7318032003280B320D2E66756E635F76657273696F6E520856657273696F6E7322260A0A675F526573706F6E736512180A074D65737361676518012001280952074D657373616765222D0A0B64656C6574655F66756E63121E0A0A66756E6374696F6E4964180120012809520A66756E6374696F6E4964227B0A0C6164645F6E65775F66756E6312160A0666756E634964180120012809520666756E63496412370A0F66756E6374696F6E56657273696F6E18022001280B320D2E66756E635F76657273696F6E520F66756E6374696F6E56657273696F6E121A0A0866756E634E616D65180320012809520866756E634E616D6522490A0973686F775F66756E6312160A0666756E634964180120012809520666756E63496412240A0D66756E6356657273696F6E4964180220012809520D66756E6356657273696F6E496422270A0D73686F775F616C6C5F66756E6312160A0666756E634964180120012809520666756E634964224A0A1873686F775F616C6C5F776974685F63726974657269615F6112120A046C616E6718012001280952046C616E67121A0A086B6579776F72647318022003280952086B6579776F726473225E0A1F73686F775F616C6C5F776974685F63726974657269615F726573706F6E736512210A0866756E6374696F6E18012001280B32052E66756E63520866756E6374696F6E12180A076D65737361676518022001280952076D65737361676532B6020A0F46756E6374696F6E4D616E6167657212280A0A6164645F6E65775F666E120D2E6164645F6E65775F66756E631A0B2E675F526573706F6E7365121F0A076164645F666E7312052E66756E631A0B2E675F526573706F6E7365280112260A0964656C6574655F666E120C2E64656C6574655F66756E631A0B2E675F526573706F6E736512240A0773686F775F666E120A2E73686F775F66756E631A0D2E66756E635F76657273696F6E122F0A0C73686F775F616C6C5F666E73120E2E73686F775F616C6C5F66756E631A0D2E66756E635F76657273696F6E300112590A1673686F775F616C6C5F776974685F637269746572696112192E73686F775F616C6C5F776974685F63726974657269615F611A202E73686F775F616C6C5F776974685F63726974657269615F726573706F6E736528013001620670726F746F33"};
}

