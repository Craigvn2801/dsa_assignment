import ballerina/grpc;


listener grpc:Listener ep = new (9090);

func[] functions = [
    {
    Id: "0",
    Name: "SqRt",
    Versions: [
    {
        Id: "1",
        Developer: {
            Name: "Amelia Earhart",
            Email: "amearheart@flight.com"
        },
        lang: "JavaScript",
        Keywords: ["square", "math", "exponentials"],
        Contents: "y = x^2"
    }, 
    {
        Id: "3",
        Developer: {
            Name: "Ferruccio Lamborghini",
            Email: "ferru@lambo.com"
        },
        lang: "C++",
        Keywords: ["square", "math", "exponentials"],
        Contents: "y = x ** 2"
    }
    ]
}
];

function findFunctionIndexId(string id) returns int? {
    int? index = ();
    foreach int i in 0 ... functions.length() - 1 {
        var func = functions[i];
        if func.Id == id {
            index = i;
            break;
        }
    }
    return index;
}

function findFunctionId(string id) returns func|error {
    int? index = findFunctionIndexId(id);
    if index != () {
        return functions[index];
    } else {
        return error(id + " does not exists.");
    }
}

function findFunctionVersionIndexId(func functi, string versionId) returns int? {
    int? indx = ();
    foreach int i in 0 ... functi.Versions.length() - 1 {
        var funcVersion = functi.Versions[i];
        if funcVersion.Id == versionId {
            indx = i;
            break;
        }
    }
    return indx;
}

function findFunctionVersionId(func func, string versionId) returns func_version|error {
    int? indx = findFunctionVersionIndexId(func, versionId);
    if indx != () {
        return func.Versions[indx];
    } else {
        return error(func.Id + " has no version with an ID of " + versionId + ".");
    }
}

function checkKeywordsMatch(string[] keywords, string[] queryKeywords) returns boolean {
    string[] lowercaseKeywords = keywords.map(k => k.toLowerAscii());
    foreach string word in queryKeywords {
        boolean exists = lowercaseKeywords.indexOf(word.toLowerAscii()) is int;
        if !exists {
            return false;
        }
    }
    return true;
}

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "FunctionManager" on ep {

    remote function add_new_fn(add_new_func value) returns g_Response|error {
        int added_fn = 0;
        int? index = findFunctionIndexId(value.funcId);
        if index == () {
            functions.push({
                Id: value.funcId,
                Name: value.funcName
            });
            added_fn += 1;
            return {
                    Message: "Successfully added new function " + value.funcId + ""
            };
        } 
        else if index >= 0 {
            func funct = functions[index];
            int? vers = findFunctionVersionIndexId(funct, value.functionVersion.Id);
            if vers == () {
                funct.Versions.push(value.functionVersion);
                return {
                    Message: "Successfully added new function/version: " + value.funcId + "/" + value.functionVersion.Id + "."
                };
            } else {
                return {
                    Message: "Failed to add new function/version " + value.funcId + "/" + value.functionVersion.Id + "."
                };
            }
        }
        return {
            Message: "Function " + value.funcId + " not be found. " + "Failed to add function."
        };
    }

    remote function delete_fn(delete_func value) returns g_Response|error {
        int? functionIndex = findFunctionIndexId(value.functionId);
        if functionIndex != () {
            _ = functions.remove(functionIndex);
            return {
                Message: "Successfully removed: " + value.functionId
            };
        } else {
            return error(value.functionId + " does not exist.");
        }
    }

    remote function show_fn(show_func value) returns func_version|error {
        func fn = check findFunctionId(value.funcId);
        func_version fnVersion = check findFunctionVersionId(fn, value.funcVersionId);
        return fnVersion;
    }

    remote function add_fns(stream<func, grpc:Error?> clientStream) returns g_Response|error {
                int addCount = 0;
        check clientStream.forEach(function(func funct) {
            if (funct.Versions.length() > 1) {

            } else {
                functions.push(funct);
                addCount += 1;
            }
        });
        return {
            Message: "Added " + addCount.toString() + " new function(s)."
        };
    }

    remote function show_all_fns(show_all_func value) returns stream<func_version, error?>|error {
        func funct = check findFunctionId(value.funcId);
        return funct.Versions.toStream();
    }
    
    remote function show_all_with_criteria(FunctionManagerShowallwithcriteriaresponseCaller caller, stream<show_all_with_criteria_a, grpc:Error?> clientStream) returns error?{
        check clientStream.forEach(function(show_all_with_criteria_a query) {
            if query.lang is string || query.keywords.length() > 0 {
                func[] matchingFunctions = [];
                foreach func func in functions {
                    foreach func_version funcVersion in func.Versions.reverse() {
                        boolean languageMatches = false;
                        if query.lang is string && query.lang.length() > 0 {
                            languageMatches = funcVersion.lang.equalsIgnoreCaseAscii(query.lang);
                        } else {
                            languageMatches = true;
                        }

                        boolean keywordsMatch = false;
                        if query.keywords.length() > 0 {
                            keywordsMatch = checkKeywordsMatch(funcVersion.Keywords, query.keywords);
                        } else {
                            keywordsMatch = true;
                        }

                        boolean versionMatches = languageMatches && keywordsMatch;
                        if versionMatches {
                            matchingFunctions.push({
                                Id: func.Id,
                                Name: func.Name,
                                Versions: [funcVersion]
                            });

                            break;
                        }
                    }
                }

                foreach var funct in matchingFunctions {
                    checkpanic caller->sendShow_all_with_criteria_response({
                        'function: funct,
                        message: "Function with language '" + query.lang + "' and keywords " + query.keywords.toString() + " found."
                    });
                }
                if matchingFunctions.length() == 0 {
                    checkpanic caller->sendShow_all_with_criteria_response({
                        message: "Functions with language '" + query.lang + "' and keywords " + query.keywords.toString() + " not found."
                    });
                }
            } else {
                checkpanic caller->sendShow_all_with_criteria_response({
                    message: "Language or keyword criteria not given."
                });
            }
        });
        check caller->complete();
    }
}