import ballerina/io;

FunctionManagerClient ep = check new ("http://localhost:9090");

function readShowAll(Show_all_with_criteriaStreamingClient streamingClient) returns error? {
    show_all_with_criteria_response? result = check streamingClient->receiveShow_all_with_criteria_response();
    int count = 0;
    while !(result is ()) {
        io:println("show_all_with_criteria result[" + count.toString() + "]:", result);
        result = check streamingClient->receiveShow_all_with_criteria_response();
        count += 1;
    }
}

public function main() returns error? {

    g_Response addNewFuncResponse = check ep->add_new_fn({
        funcId: "0",
        functionVersion: 
        {
            Id: "4",
            Developer: {
                Name: "Amy Weathers",
                Email: "aweathers@hotmail.com"
            },
            lang: "C#",
            Keywords: ["square", "exponentials"],
            Contents: "y = x * x"
        }
    });
    io:println("add_new_fn1: ", addNewFuncResponse.Message);
    g_Response addNewFuncResponse2 = check ep->add_new_fn({
        funcId: "6",
        funcName: "Pi"
    });
    io:println("add_new_fn2: ", addNewFuncResponse2.Message);

    Add_fnsStreamingClient addFnsClient = check ep->add_fns();
    
    func[] functionsToAdd = [
    {
        Id: "2",
        Name: "Add",
        Versions: [
        {
            Id: "4",
            Developer: {
                Name: "Martha Marks",
                Email: "martiemc@gmail.com"
            },
            Keywords: ["add", "math"],
            lang: "Julia",
            Contents: "x + y"
        }, 
                        {
            Id: "5",
            Developer: {
                Name: "Bobby Cohen Smith",
                Email: "bcs@gmail.com"
            },
            Keywords: ["add", "math"],
            lang: "R",
            Contents: "x + y + z"
        }
        ]
    }, 
    {
        Id: "3",
        Name: "Subtract",
        Versions: []
    }
    ];
    foreach var fn in functionsToAdd {
        check addFnsClient->sendFunc(fn);
    }
    check addFnsClient->complete();
    g_Response? addFnsResponse = check addFnsClient->receiveG_Response();
    if addFnsResponse != () {
        io:println("add_fns response = ", addFnsResponse.Message);
    }

    var response = check ep->delete_fn({functionId: "3"});
    io:println("delete_fn: ", response.Message);

    func_version functionVersion = check ep->show_fn({funcId: "0", funcVersionId: "3"});
    io:println("show_fn: ", functionVersion);

    var showAllFnsClient = check ep->show_all_fns({funcId: "0"});
    check showAllFnsClient.forEach(function(func_version fnVersion) {
        io:println("show_all_fns: ", fnVersion);
    });

    Show_all_with_criteriaStreamingClient queryClient = check ep->show_all_with_criteria();
    future<error?> f1 = start readShowAll(queryClient);
    show_all_with_criteria_a[] queries = [
        {lang: "JavaScript"}, 
        {lang: "JavaScript", keywords: ["math", "square"]}, 
        {keywords: ["math", "square"]}, 
        {keywords: ["math", "circle"]}, 
        {lang: "Python"}, 
        {lang: "C++"}
    ];
    foreach var query in queries {
        check queryClient->sendShow_all_with_criteria_a(query);
    }
    check queryClient->complete();
    check wait f1;
}

